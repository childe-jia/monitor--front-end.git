## 监控前端案例

`npm  i` 下载依赖
`npm run serve` 启动项目

## 监控后台服务器

`npm  i` 下载依赖
`npm run dev` 启动服务

### 启动数据库

    下载Docker

`$ docker pull mongo:latest` 取最新版的 MongoDB 镜像

`$ docker images` 查看本地镜像

`docker run -d -p 27017:27017 --name my-mongo-con` 运行容器

安装完成后，我们可以使用以下命令来运行 mongo 容器：

```js
docker run -d -p 27017:27017 --name my-mongo-container mongo
```

参数说明：

-d: 后台运行容器。
-p 27017:27017: 将主机的 27017 端口映射到容器的 27017 端口。
--name my-mongo-container: 为容器指定一个名字，这里是 my-mongo-container，你可以根据需要更改

5、安装成功
最后我们可以通过 docker ps 命令查看容器的运行信息：

记得在不再需要时停止和删除容器，可以使用以下命令

```
docker stop my-mongo-container
docker rm my-mongo-container
```

## 监控后台

`npm  i` 下载依赖
`npm run start` 启动项目
