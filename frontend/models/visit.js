const mongoose = require("mongoose");

const pvSchema = new mongoose.Schema({
  appId: {
    // 应用id
    type: String,
    required: true,
  },
  // 用户id
  userId: {
    type: String,
    required: true,
  },
  // 停留时间
  stayTime: {
    type: Number,
    required: true,
  },
  // 创建时间
  createTime: {
    type: Number,
    required: true,
  },
  // 当前页面
  currentPage: {
    type: String,
    required: false,
  },
  // 浏览器信息
  ua: {
    type: String,
    required: true,
  },
});

module.exports = mongoose.model("pv", pvSchema);
