const ErrorLog = require("./error");
const UserAction = require("./userAction");
const VisitLog = require("./visit");
const UserLog = require("./userLog");
module.exports = {
  ErrorLog,
  UserAction,
  VisitLog,
  UserLog,
};
