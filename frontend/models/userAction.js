const mongoose = require("mongoose");

const userActionSchema = new mongoose.Schema(
  //项目的appId
  {
    appId: {
      type: String,
      required: true,
    },
    // 用户id
    userId: {
      type: String,
      required: true,
    },
    // 用户行为类型
    actionType: {
      type: String,
      required: true,
    },
    // 用户行为数据
    data: {
      type: String,
      default: "",
    },
    // 用户行为时间
    createTime: {
      type: Number,
      required: true,
    },
    // 当前页面
    currentPage: {
      type: String,
      required: true,
    },
    // ua信息 用户浏览器信息
    ua: {
      type: String,
      required: true,
    },
  }
);

module.exports = mongoose.model("UserAction", userActionSchema);
