const mongoose = require("mongoose");

const errorLogSchema = new mongoose.Schema({
  //项目的appId
  appId: {
    type: String,
    required: true,
  },
  // 用户id
  userId: {
    type: String,
    required: true,
  },
  // 错误类型
  errorType: {
    type: String,
    required: true,
  },
  // 错误信息
  errMsg: {
    type: String,
    default: "",
  },
  // 错误行数
  col: {
    type: Number,
    required: false,
  },
  // 错误列数
  row: {
    type: Number,
    required: false,
  },
  //创建时间
  createTime: {
    type: Number,
    required: true,
  },
  //当前页面
  currentPage: {
    type: String,
    required: true,
  },
  // ua信息 用户浏览器信息
  ua: {
    type: String,
    required: true,
  },
});

module.exports = mongoose.model("errorlog", errorLogSchema);
