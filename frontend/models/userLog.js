const mongoose = require("mongoose");

const userLog = new mongoose.Schema({
  // appid
  appId: {
    type: String,
    required: true,
  },
  // 用户id
  userId: {
    type: String,
    required: true,
  },
  // 浏览器信息
  ua: {
    type: String,
    required: true,
  },
});

module.exports = mongoose.model("userlogs", userLog);
