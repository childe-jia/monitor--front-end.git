import { lazyReport } from "./report";
export function errorTrackerReport() {
  // --------  js error ---------
  console.log(window);
  const originOnError = window.onerror;
  window.onerror = function (msg, url, row, col, error) {
    // 处理原有的onerror
    if (originOnError) {
      originOnError.call(window, msg, url, row, col, error);
    }
    console.log("异步错误的话我们可以用window.onerror来进行处理，", msg);
    // 错误上报
    lazyReport("error", {
      message: msg,
      file: url,
      row,
      col,
      error,
      errorType: "jsError",
    });
  };

  // ------  promise error  --------
  window.addEventListener("unhandledrejection", (error) => {
    console.log(
      "unhandledrejection，用来监听没有被捕获的promise错误。",
      error.reason
    );
    lazyReport("error", {
      message: error.reason,
      error,
      errorType: "promiseError",
    });
  });

  // ------- resource error --------
  window.addEventListener(
    "error",
    (error) => {
      let target = error.target;
      let isElementTarget =
        target instanceof HTMLScriptElement ||
        target instanceof HTMLLinkElement ||
        target instanceof HTMLImageElement;
      if (!isElementTarget) {
        return; // js error不再处理
      }
      console.log("加载 " + target.tagName + " 资源错误");
      lazyReport("error", {
        message: "加载 " + target.tagName + " 资源错误",
        file: target.src,
        errorType: "resourceError",
      });
    },
    true
  );
}
/**
 * 手动捕获错误
 */
export function errorCaptcher(error, msg) {
  // 上报错误
  lazyReport("error", {
    message: msg,
    error: error,
    errorType: "catchError",
  });
}
