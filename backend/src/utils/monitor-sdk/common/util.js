import { errorTrackerReport } from "./errorTracker";
import { autoTrackerReport } from "./actionTracker";
import { hashPageTrackerReport, historyPageTrackerReport } from "./pageTracker";
/**
 * 加载配置
 * @param {*} options
 */
export function loadConfig(options) {
  const {
    appId, // 系统id
    userId, // 用户id
    reportUrl, // 后端url
    autoTracker, // 自动埋点
    delay, // 延迟和合并上报的功能
    hashPage, // 是否hash录有
    errorReport, // 是否开启错误监控
  } = options;

  // --------- appId ----------------
  if (appId) {
    window["_monitor_app_id_"] = appId;
  }

  // --------- userId ----------------
  if (userId) {
    window["_monitor_user_id_"] = userId;
  }

  // --------- 服务端地址 ----------------
  if (reportUrl) {
    window["_monitor_report_url_"] = reportUrl;
  }

  // -------- 合并上报的间隔 ------------
  if (delay) {
    window["_monitor_delay_"] = delay;
  }

  // --------- 是否开启错误监控 ------------
  if (errorReport) {
    errorTrackerReport();
  }

  // --------- 是否开启无痕埋点 ----------
  if (autoTracker) {
    autoTrackerReport();
  }

  // ----------- 路由监听 --------------
  if (hashPage) {
    hashPageTrackerReport(); // hash路由上报
  } else {
    historyPageTrackerReport(); // history路由上报
  }
}

/**
 * 获取元素的XPath
 *
 * 该函数旨在通过递归方式构建给定元素的XPath字符串。它首先检查元素是否有id，
 * 如果有，则直接使用id构建XPath；如果元素是body元素，直接返回其标签名。
 * 对于其他情况，它通过遍历元素的同级兄弟元素来确定元素在兄弟中的位置，
 * 从而构建XPath。
 *
 * @param {Element} element - 需要获取XPath的DOM元素
 * @returns {string} 元素的XPath字符串
 * //*[@id="app"]/DIV[1]/BUTTON[3]
 */
export function getPathTo(element) {
  // 特殊情况处理：如果元素没有父元素，返回空字符串
  if (!element.parentNode) return "";

  // 如果元素有id，则直接通过id构建XPath
  if (element.id) return `//*[@id="${element.id}"]`;

  // 如果元素是body元素，直接返回其标签名作为XPath
  if (element === document.body) return element.tagName;

  // 存储父元素和兄弟元素索引
  const parent = element.parentNode;
  let index = 1;

  // 遍历兄弟元素，构建XPath
  for (let sibling of parent.children) {
    if (sibling === element) {
      // 构建XPath
      return `${getPathTo(parent)}/${element.tagName}[${index}]`;
    } else if (sibling.tagName === element.tagName) {
      // 如果是同标签名的兄弟元素，增加索引计数
      index++;
    }
  }
}
