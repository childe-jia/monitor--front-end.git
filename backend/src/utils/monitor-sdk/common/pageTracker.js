import { lazyReport } from "./report";
/**
 * history路由监听
 */
export function historyPageTrackerReport() {
  let beforeTime = Date.now(); // 进入页面的时间

  let beforePage = ""; // 上一个页面

  // 获取在某个页面的停留时间
  function getStayTime() {
    let curTime = Date.now();
    // 停留时间 = 当前时间 - 进入页面的时间
    let stayTime = curTime - beforeTime;
    // 重置时间
    beforeTime = curTime;
    return stayTime;
  }

  //上报数据
  function listener() {
    const stayTime = getStayTime(); // 停留时间
    const currentPage = window.location.href; // 页面路径
    //上报
    lazyReport("visit", {
      stayTime,
      page: beforePage,
    });
    beforePage = currentPage;
  }

  // 直接调用 history.pushState() 或 history.replaceState() 方法本身不会触发 popstate 事件
  const createHistoryEvent = function (name) {
    // 拿到原来的处理方法
    const origin = window.history[name];
    return function (event) {
      //  避免在路径没有发生变化的情况下触发自定义事件。
      if (name === "replaceState") {
        const { current } = event;
        // 它会首先检查当前的路径是否和要替换的路径相同，
        const pathName = location.pathname;
        // 如果相同的话，就会直接执行原始的 replaceState 方法，并返回结果。
        if (current === pathName) {
          let res = origin.apply(this, arguments);
          return res;
        }
      }
      let res = origin.apply(this, arguments); ////执行history函数
      let e = new Event(name); //声明自定义事件
      e.arguments = arguments;
      window.dispatchEvent(e); //抛出事件
      return res; //返回方法，用于重写history的方法
    };
  };

  // history.pushState()  用于在浏览器的历史记录堆栈中添加
  window.history.pushState = createHistoryEvent("pushState");
  //  history.replaceState() 加或替换一个状态
  window.history.replaceState = createHistoryEvent("replaceState");

  // 当history对象发生变化时，就会触发popState事件
  // history.go() 跳转到历史记录的某一个页面
  // history.back() 返回上一个页面，和浏览器的退回功能一样
  // history.forward() 前进一页，和浏览器前进功能呢一样
  window.addEventListener("popstate", function () {
    console.log("popstatec触发");
    listener();
  });

  // 页面load监听
  window.addEventListener("load", function () {
    console.log("页面load监听==load");
    listener();
  });
  // unload监听
  window.addEventListener("unload", function () {
    console.log("页面卸载 unload监听");
    listener();
  });

  //
  window.addEventListener("pushState", function () {
    console.log("history.pushState");
    listener();
  });

  // history.replaceState
  window.addEventListener("replaceState", function () {
    console.log("history.replaceState");
    listener();
  });
}

/**
 * hash路由监听
 */
export function hashPageTrackerReport() {
  let beforeTime = Date.now(); // 进入页面的时间
  let beforePage = ""; // 上一个页面

  function getStayTime() {
    let curTime = Date.now();
    let stayTime = curTime - beforeTime;
    beforeTime = curTime;
    return stayTime;
  }

  function listener() {
    const stayTime = getStayTime();
    const currentPage = window.location.href;
    lazyReport("visit", {
      stayTime,
      page: beforePage,
    });
    beforePage = currentPage;
  }
  const createHistoryEvent = function (name) {
    const origin = window.history[name];
    return function (event) {
      if (name === "replaceState") {
        const { current } = event;
        const pathName = location.pathname;
        if (current === pathName) {
          let res = origin.apply(this, arguments);
          return res;
        }
      }

      let res = origin.apply(this, arguments);
      let e = new Event(name);
      e.arguments = arguments;
      window.dispatchEvent(e);
      return res;
    };
  };
  // 页面load监听
  window.addEventListener("load", function () {
    console.log("load");
    listener();
  });

  // hash路由监听 只可以监听到点击浏览器按钮 （因为vue内部还是使用了history）
  window.addEventListener("hashchange", function () {
    console.log("hashchange");
    listener();
  });

  window.history.pushState = createHistoryEvent("pushState");
  // history.pushState
  window.addEventListener("pushState", function () {
    console.log(" history.pushState");
    listener();
  });
}
